var express = require('express');
var Promise = require("bluebird");
var router = express.Router();
var fs = require('fs');
var jwt = require('jsonwebtoken');

Promise.promisifyAll(fs);

// When running locally, app-registry will look for sibling folders with name {portal}
// to serve components from
router.get('/:tenant/:path(*)', function(req, res) {
    var tenant = req.params.tenant;

    var localPath = __dirname + '/../app/components/';

    var lendfoundryPath = localPath + 'lendfoundry/' + req.params.path;
    var tenantPath = localPath + tenant + '/' + req.params.path;
    
    fs.accessAsync(tenantPath)
        .then(function() {
            return fs.readFileAsync(tenantPath);
        })
        .then(function(html) {
            res.type('html').send(html);
        }).error(function() {

            fs.accessAsync(lendfoundryPath)
                .then(function() {
                    return fs.readFileAsync(lendfoundryPath);
                })
                .then(function(html) {
                    res.type('html').send(html);
                })
                .error(function() {
                    res.status(404).send("File not found.");
                });

        })
});

module.exports = router;