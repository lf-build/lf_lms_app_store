FROM node:latest

ADD /server /tmp/server
ADD /app /tmp/app
ADD /package.json /tmp/package.json
WORKDIR /tmp

RUN npm install

EXPOSE 5000

ENTRYPOINT environment=${environment} node /tmp/server/server.js